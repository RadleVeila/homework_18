

#include <iostream>
#include <string>
#include <stack>
using namespace std;


int main()
{
    
    stack <int> steck;
    int o = 0;

    cout << "Enter 6 different value" << endl;
    cout << "Example: 2,5,1,9,7,3" << endl;

    while (o != 6)
    {
        int  a;
        cin >> a;

        steck.push(a);
        o++;
    }
    if (steck.empty()) cout << "Stack is not empty";

    cout << "Top stack element" << " "<<steck.top() << endl;
    cout << "Delete Top element" << endl;

    steck.pop();
    cout << "This is new element" <<" "<< steck.top()<<endl;

    system("pause");
    
    return 0;
}

